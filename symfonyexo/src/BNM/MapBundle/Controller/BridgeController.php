<?php

namespace BNM\MapBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

use BNM\MapBundle\Entity\Bridge;

class BridgeController extends Controller
{
    /**
     * @Route("/get")
     */
    public function getAction($id)
    {
        $em=$this->getDoctrine()->getManager();
        $bridge = $em->getRepository('BNMMapBundle:Bridge')->find($id);
        if(!is_null($bridge)){
            $bridge = $this->get('jms_serializer')->serialize($bridge,'json');
            $response = new Response($bridge);
            $response->headers->set('Content-type','application/json');
        }
        else{
            $response = new Response('Aucun bridge trouvé');
        }

        return $response;
    }

    /**
     * @Route("/list")
     */
    public function listAction()
    {
        $em=$this->getDoctrine()->getManager();
        $bridges = $em->getRepository('BNMMapBundle:Bridge')->findAll();
        if (count($bridges)>0) {
            # code...
            $bridges = $this->get('jms_serializer')->serialize($bridges,'json');
            $response = new Response($bridges);
            $response->headers->set('Content-type','application/json');
        }
        else{
            $response = new Response('Aucun device trouvé');
        }

        return $response;
    }

    /**
     * @Route("/add")
     */
    public function addAction(Request $request)
    {
        if ($request->isMethod('POST')) {
            # code...
            $request=json_decode($request->getContent(),true);

            $bridge = new Bridge();
            $bridge->setName($request['name']);
            $bridge->setBridgeGuid($request['guid']);
            $bridge->setERange($request['range']);
            $bridge->setProtocol($request['protocol']);

            $em=$this->getDoctrine()->getManager();
            $em->persist($bridge);

            $em->flush();

            return new Response('Bridge added');
        }
        else{
            return new Response('Bad request method');
        }
    }

    /**
     * @Route("/edit")
     */
    public function editAction($id,Request $request)
    {
        return $this->render('BNMMapBundle:Bridge:edit.html.twig', array(
            // ...
        ));
    }

    /**
     * @Route("/delete")
     */
    public function deleteAction($id,Request $request)
    {
        return $this->render('BNMMapBundle:Bridge:delete.html.twig', array(
            // ...
        ));
    }

}

<?php

namespace BNM\MapBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('BNMMapBundle:Default:index.html.twig');
    }
}

<?php

namespace BNM\MapBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

use BNM\MapBundle\Entity\Device;

class DeviceController extends Controller
{
    /**
     * @Route("/get")
     */
    public function getAction($id)
    {
        $em=$this->getDoctrine()->getManager();
        $device = $em->getRepository('BNMMapBundle:Device')->find($id);
        if(!is_null($device)){
            $device = $this->get('jms_serializer')->serialize($device,'json');
            $response = new Response($device);
            $response->headers->set('Content-type','application/json');
        }
        else{
            $response = new Response('Aucun device trouvé');
        }

        return $response;
    }

    /**
     * @Route("/list")
     */
    public function listAction()
    {
        $em=$this->getDoctrine()->getManager();
        $devices = $em->getRepository('BNMMapBundle:Device')->findAll();
        
        //if (count($devices)>0 && !is_null($devices)) {
            # code...
            $devices = $this->get('jms_serializer')->serialize($devices,'json');
            $response = new Response($devices);
            $response->headers->set('Content-type','application/json');
        /*}
        else{
            $response = new Response('Aucun device trouvé');
        }*/

        return $response;
    }

    /**
     * @Route("/add")
     */
    public function addAction(Request $request)
    {
        if ($request->isMethod('POST')) {
            # code...
            $req=json_decode($request->getContent(),true);

            $device = new Device();
            $device->setName($req['name']);
            $device->setDeviceGuid($req['guid']);
            $device->setProtocol($req['protocol']);

            $em=$this->getDoctrine()->getManager();
            $em->persist($device);

            $em->flush();

            return new Response('Device added');
        }
        else{
            return new Response('Bad request method');
        }

        
    }

    /**
     * @Route("/edit")
     */
    public function editAction($id,Request $request)
    {
        if ($request->isMethod('PUT')) {
            $req=json_decode($request->getContent(),true);
            //return new Response($req["bridge_id"]);
            $em=$this->getDoctrine()->getManager();
            $device = $em->getRepository('BNMMapBundle:Device')->find($id);
            $bridge = $em->getRepository('BNMMapBundle:Bridge')->find($req["bridge"]);
            $device->setBridge($bridge);

            $em->persist($device);
            $em->persist($bridge);
            $em->flush();
            
            return new Response('Device linked');
        }
        else{
            return new Response('Bad request method');
        }
    }

    /**
     * @Route("/delete")
     */
    public function deleteAction($id,Request $request)
    {
        
    }

}

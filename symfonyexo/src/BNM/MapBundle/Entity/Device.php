<?php

namespace BNM\MapBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Device
 *
 * @ORM\Table(name="device")
 * @ORM\Entity(repositoryClass="BNM\MapBundle\Repository\DeviceRepository")
 */
class Device
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var guid
     *
     * @ORM\Column(name="device_guid", type="guid", unique=true)
     */
    private $deviceGuid;

    /**
     * @var string
     *
     * @ORM\Column(name="protocol", type="string", length=255)
     */
    private $protocol;

    /**
     *@ORM\ManyToOne(targetEntity="Bridge",inversedBy="devices")
     *@ORM\JoinColumn(nullable=true, name="bridge_id", referencedColumnName="id")
     */
    private $bridge;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Device
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set deviceGuid
     *
     * @param guid $deviceGuid
     *
     * @return Device
     */
    public function setDeviceGuid($deviceGuid)
    {
        $this->deviceGuid = $deviceGuid;

        return $this;
    }

    /**
     * Get deviceGuid
     *
     * @return guid
     */
    public function getDeviceGuid()
    {
        return $this->deviceGuid;
    }

    /**
     * Set protocol
     *
     * @param string $protocol
     *
     * @return Device
     */
    public function setProtocol($protocol)
    {
        $this->protocol = $protocol;

        return $this;
    }

    /**
     * Get protocol
     *
     * @return string
     */
    public function getProtocol()
    {
        return $this->protocol;
    }

    /**
     * Set bridge
     *
     * @param \BNM\MapBundle\Entity\Bridge $bridge
     *
     * @return Device
     */
    public function setBridge(\BNM\MapBundle\Entity\Bridge $bridge = null)
    {
        $this->bridge = $bridge;

        return $this;
    }

    /**
     * Get bridge
     *
     * @return \BNM\MapBundle\Entity\Bridge
     */
    public function getBridge()
    {
        return $this->bridge;
    }
}

<?php

namespace BNM\MapBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Bridge
 *
 * @ORM\Table(name="bridge")
 * @ORM\Entity(repositoryClass="BNM\MapBundle\Repository\BridgeRepository")
 */
class Bridge
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var guid
     *
     * @ORM\Column(name="bridge_guid", type="guid", unique=true)
     */
    private $bridgeGuid;

    /**
     * @var int
     *
     * @ORM\Column(name="e_range", type="integer")
     */
    private $eRange;

    /**
     * @var string
     *
     * @ORM\Column(name="protocol", type="string", length=255)
     */
    private $protocol;


    /**
     * @ORM\OneToMany(targetEntity="Device",mappedBy="bridge")
     */
    private $devices;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Bridge
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set bridgeGuid
     *
     * @param guid $bridgeGuid
     *
     * @return Bridge
     */
    public function setBridgeGuid($bridgeGuid)
    {
        $this->bridgeGuid = $bridgeGuid;

        return $this;
    }

    /**
     * Get bridgeGuid
     *
     * @return guid
     */
    public function getBridgeGuid()
    {
        return $this->bridgeGuid;
    }

    /**
     * Set eRange
     *
     * @param integer $eRange
     *
     * @return Bridge
     */
    public function setERange($eRange)
    {
        $this->eRange = $eRange;

        return $this;
    }

    /**
     * Get eRange
     *
     * @return int
     */
    public function getERange()
    {
        return $this->eRange;
    }

    /**
     * Set protocol
     *
     * @param string $protocol
     *
     * @return Bridge
     */
    public function setProtocol($protocol)
    {
        $this->protocol = $protocol;

        return $this;
    }

    /**
     * Get protocol
     *
     * @return string
     */
    public function getProtocol()
    {
        return $this->protocol;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->devices = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add device
     *
     * @param \BNM\MapBundle\Entity\Device $device
     *
     * @return Bridge
     */
    public function addDevice(\BNM\MapBundle\Entity\Device $device)
    {
        $this->devices[] = $device;

        return $this;
    }

    /**
     * Remove device
     *
     * @param \BNM\MapBundle\Entity\Device $device
     */
    public function removeDevice(\BNM\MapBundle\Entity\Device $device)
    {
        $this->devices->removeElement($device);
    }

    /**
     * Get devices
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDevices()
    {
        return $this->devices;
    }
}

import { Component, OnInit } from '@angular/core';
import { DeviceService } from '../device.service';

@Component({
  selector: 'app-device-list',
  templateUrl: './device-list.component.html',
  styleUrls: ['./device-list.component.css']
})
export class DeviceListComponent implements OnInit {

  private url = 'devices';
  devices: any = [];

  constructor(
  	private deviceService: DeviceService,
  ) { }

  ngOnInit() {

  	this.deviceService.getDevices(this.url).subscribe(
  		devices => this.devices = devices,
  		error => {this.devices = null, console.log("Erreur lors de la requête")}
  	)

  }

}

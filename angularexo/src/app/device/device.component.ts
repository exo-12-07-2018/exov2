import { Component, OnInit } from '@angular/core';

import { DeviceService } from '../device.service';
import { BridgeService } from '../bridge.service';

import { ActivatedRoute, Router } from '@angular/router';

import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-device',
  templateUrl: './device.component.html',
  styleUrls: ['./device.component.css']
})
export class DeviceComponent implements OnInit {
	
  private deviceUrl = 'device/';
  private bridgesUrl = 'bridges';
  private updateDeviceUrl = 'device/edit/';
  private device: any = [];
  private bridges: any = [];

  constructor(
  	private deviceService: DeviceService,
  	private bridgeService: BridgeService,
  	private route: ActivatedRoute,
  	private router: Router
  ) { }

  ngOnInit() {
  	const id = this.route.snapshot.params['id'];

  	this.deviceService.getDevice(this.deviceUrl,id).subscribe(
  		device => this.device = device,
  		error => console.log("Erreur lors de la requ�te")
  	)
  	this.bridgeService.getBridges(this.bridgesUrl).subscribe(
  		bridges => this.bridges = bridges,
  		error => console.log("Erreur lors de la requ�te")
  	)
  }


  linkWithBridge(linkdeviceform: NgForm){
    
    const bridge_id = linkdeviceform.value
    this.deviceService.associateWithBridge(this.updateDeviceUrl,this.device.id,bridge_id);
    this.router.navigate(['/devices'])
  }
}


 
import { Component, OnInit } from '@angular/core';
import { BridgeService } from '../bridge.service'

@Component({
  selector: 'app-bridge-list',
  templateUrl: './bridge-list.component.html',
  styleUrls: ['./bridge-list.component.css']
})
export class BridgeListComponent implements OnInit {
	
  private bridges: any = [];
  private url = 'bridges';

  constructor(
  	private bridgeService: BridgeService, 
  ) { }

  ngOnInit() {
  
  	this.bridgeService.getBridges(this.url).subscribe(
  		bridges => this.bridges = bridges,
  		error => {this.bridges = null , console.log("Erreur lors de la requête")} 
  	)

  }

}

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class DeviceService {

  constructor(private httpClient: HttpClient) { }

  getDevice(url,id){
  	return this.httpClient.get(environment.endPoint + url + id);
  }

  getDevices(url){
  	return this.httpClient.get(environment.endPoint + url);
  }

  addDevice(url,device){
    
  	return this.httpClient.post(environment.endPoint + url,device).subscribe()
  }

  associateWithBridge(url,id,bridge){
    return this.httpClient.put(environment.endPoint + url + id,bridge).subscribe()

  }
}

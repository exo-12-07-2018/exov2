import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';



//Components
import { AppComponent } from './app.component';
import { DeviceListComponent } from './device-list/device-list.component';
import { BridgeListComponent } from './bridge-list/bridge-list.component';
import { BridgeAddComponent } from './bridge-add/bridge-add.component';
import { DeviceAddComponent } from './device-add/device-add.component';
import { DeviceComponent } from './device/device.component';
import { BridgeComponent } from './bridge/bridge.component';
import { NotFoundComponent } from './not-found/not-found.component';


const routes: Routes = [
  { path : 'device/detail/:id', component : DeviceComponent },
  { path : 'devices', component : DeviceListComponent  },
  { path : 'device/add', component : DeviceAddComponent },

  { path : 'bridge/detail/:id', component : BridgeComponent },
  { path : 'bridges', component : BridgeListComponent  },
  { path : 'bridge/add', component : BridgeAddComponent },
  { path : 'not-found', component : NotFoundComponent}

];


@NgModule({
  declarations: [
    AppComponent,
    DeviceListComponent,
    BridgeListComponent,
    BridgeAddComponent,
    DeviceAddComponent,
    DeviceComponent,
    BridgeComponent,
    NotFoundComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule.forRoot(routes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

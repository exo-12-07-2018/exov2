import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class BridgeService {

  constructor(private httpClient: HttpClient) { }

  getBridge(url,id){
  	return this.httpClient.get(environment.endPoint + url + id);
  }

  getBridges(url){
  	return this.httpClient.get(environment.endPoint + url);
  }

  addBridge(url,bridge){
  	return this.httpClient.post(environment.endPoint + url,bridge).subscribe();
  }
}

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, ReactiveFormsModule, FormGroup,Validators } from '@angular/forms';

import { DeviceService } from '../device.service';
import { Device } from '../models/Device.model';

@Component({
  selector: 'app-device-add',
  templateUrl: './device-add.component.html',
  styleUrls: ['./device-add.component.css']
})
export class DeviceAddComponent implements OnInit {
  deviceform: FormGroup;
  private url = 'device/add';
  
  constructor(
  	private formBuilder: FormBuilder,
  	private deviceService: DeviceService,
  	private router: Router,
  ) { }

  ngOnInit() {
  	this.initdeviceform();
  }

  initdeviceform(){
  	this.deviceform = this.formBuilder.group({
  		name : [''],
  		guid : [''],
  		protocol : [''],


  	})
  }


  onSubmit(){
  	const formValue = this.deviceform.value;
  	const newDevice = new Device(
  		formValue['name'],
  		formValue['guid'],
  		formValue['protocol'],
  	); 

  	this.deviceService.addDevice(this.url,newDevice)
  	this.router.navigate(['/devices'])
  }	  

}

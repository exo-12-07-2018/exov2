import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, ReactiveFormsModule, FormGroup,Validators } from '@angular/forms';

import { BridgeService } from '../bridge.service';
import { Bridge } from '../models/Bridge.model';

@Component({
  selector: 'app-bridge-add',
  templateUrl: './bridge-add.component.html',
  styleUrls: ['./bridge-add.component.css']
})
export class BridgeAddComponent implements OnInit {
  bridgeform: FormGroup;
  private url = 'bridge/add';
  constructor(
  	private formBuilder: FormBuilder,
  	private bridgeService: BridgeService,
  	private router: Router,
  ) { }

  ngOnInit() {
  	this.initbridgeform();

  }

  initbridgeform(){
  	this.bridgeform = this.formBuilder.group({
  		name : [''],
  		guid : [''],
  		range : [''],
  		protocol : [''],


  	})
  }

  onSubmit(){
  	const formValue = this.bridgeform.value;
  	const newBridge = new Bridge(
  		formValue['name'],
  		formValue['guid'],
  		formValue['range'],
  		formValue['protocol'],
  	); 

  	this.bridgeService.addBridge(this.url,newBridge)
  	this.router.navigate(['/bridges'])
  }	 

}

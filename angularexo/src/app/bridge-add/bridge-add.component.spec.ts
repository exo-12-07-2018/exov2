import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BridgeAddComponent } from './bridge-add.component';

describe('BridgeAddComponent', () => {
  let component: BridgeAddComponent;
  let fixture: ComponentFixture<BridgeAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BridgeAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BridgeAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
